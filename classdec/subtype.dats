#include  "share/atspre_define.hats"
#include  "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"

macdef showtype (x) = $showtype ,(x)
macdef showlvaltype (x) = pridentity ($showtype ,(x))

classdec CAT1
classdec CAT2 : CAT1
classdec CAT3

// gobj (class,type) => 

abstype AT1 ( c : cls) = ptr //'(int)

stadef C1 = CAT1
typedef C1 = [c : cls | c <= C1] AT1 (c)

stadef C2 = CAT2
typedef C2 = [c : cls | c <= C2] AT1 (c)

 
extern
fun make_c1 () : AT1 (CAT1) = "mac#"

extern
fun make_c2 () : AT1 (CAT2) = "mac#" //C2


(* extern *)
(* fun take_c1 {c:cls | c <= CAT1} (pc : AT1 (c) ) : void *)
(* extern *)
(* fun take_c2 {c:cls | c <= CAT2} (pc : AT1 (c) ) : void *)


extern
fun take_c1  (pc : C1 ) : void = "mac#"
extern
fun take_c2  (pc : C2 ) : void = "mac#"


implement main0 () = {
   val c1 = make_c1 ()
   val c2 = make_c2 ()
   (* val _ = showtype (c1) *)
   (* val _ = showtype (c2) *)
   val () = take_c1 (c1)
   val () = take_c1 (c2)
   val () = take_c2 (c2)
   (* val () = take_c2 (c1) *)
}

