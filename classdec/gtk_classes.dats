#include  "share/atspre_define.hats"
#include  "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"

(*

patscc -o hello gtk_classes.dats $(pkg-config --cflags --libs gtk+-3.0)
patscc -t -c gtk_classes.dats $(pkg-config --cflags  gtk+-3.0)
*)



%{^
#include <gtk/gtk.h>
%}
%{^
typedef void (*GCallback) (void) ; 
typedef char ** charptrptr ;

%};


abstype charptrptr = $extype"charptrptr"
classdec GObject_cls
classdec  GInitiallyUnowned_cls : GObject_cls
classdec    GtkWidget_cls : GInitiallyUnowned_cls
classdec      GtkContainer_cls : GtkWidget_cls
classdec       GtkBin_cls : GtkContainer_cls
classdec        GtkWindow_cls : GtkBin_cls


classdec GtkButton_cls : GtkWindow_cls

absvtype gobj_vtype (c : cls) = ptr
vtypedef vgobj (c : cls) = gobj_vtype (c)

vtypedef GObject = [c:cls | c <= GObject_cls] vgobj (c)
vtypedef GtkWidget = [c:cls | c <= GtkWidget_cls] vgobj (c)
vtypedef GtkContainer = [c:cls | c <= GtkContainer_cls] vgobj (c)
vtypedef GtkWindow = [c:cls | c <= GtkWindow_cls] vgobj (c)
vtypedef GtkButton = [c:cls | c <= GtkButton_cls] vgobj (c)


extern
fun gtk_init (&int,  &charptrptr) : void = "mac#gtk_init"

macdef GTK_WINDOW_TOPLEVEL = $extval(int , "GTK_WINDOW_TOPLEVEL")

extern
fun gtk_window_new (t: int) : GtkWindow = "mac#gtk_window_new"

extern
fun gtk_main () : void = "mac#gtk_main"


fun dummy_free_res{c:cls | c <= GObject_cls}
(g : vgobj (c) ) : void = {
  val _ = $UN.castvwtp0{ptr}(g)
}


  

macdef NULL = $extval(ptr , "NULL")
typedef GCallback = $extype"GCallback" 

extern
fun g_signal_connect (!GObject,string,GCallback,ptr) : void
= "mac#g_signal_connect"

extern
fun gtk_widget_show (!GtkWidget) : void = "mac#gtk_widget_show"

extern
fun gtk_main_quit () : void = "mac#gtk_main_quit"

extern
fun gtk_button_new_with_label (label : string) : GtkButton = "mac#"

extern
fun gtk_container_set_border_width
(c : !GtkContainer, w : int) : void = "mac#"

extern
fun g_signal_connect_swapped(!GObject,string,GCallback,ptr) : void
="mac#"

extern
fun gtk_widget_destroy (GtkWidget) : void = "mac#"

extern
fun gtk_container_add (!GtkContainer,!GtkWidget) : void = "mac#"

extern
fun gtk_window_set_title (!GtkWindow,string) : void = 
"mac#"
fun print_hello (widget : !GtkWidget , data : ptr) : void = let
  val () = println! ("Hello World!")
  in end


implement main0 (argc,argv) = {
  val () = println! ("hello world!")
  //val () = print_version()
  var argc : int = argc
  var argv : charptrptr = $UN.castvwtp1{charptrptr}(argv)
  val () = gtk_init (argc,argv) //addr@(argv))  
  val window = gtk_window_new(GTK_WINDOW_TOPLEVEL)
  val () = g_signal_connect
           (window,"destroy",
            $UN.cast{GCallback}(gtk_main_quit),NULL)
  
  val () = gtk_container_set_border_width (window,10)
  val button = gtk_button_new_with_label ("Hello world!")
  val () = g_signal_connect 
          (button,"clicked",$UN.cast{GCallback}print_hello,
          NULL)
          
  val () = g_signal_connect_swapped 
           (button,"clicked",$UN.cast{GCallback}(gtk_widget_destroy),
            $UN.castvwtp1{ptr}(window))
   
  val () = gtk_container_add(window,button) 
  val () = gtk_widget_show (button)                  
  val () = gtk_widget_show (window)
  val () = dummy_free_res (window)
  val () = dummy_free_res (button)
  val () = gtk_main () 
  
  
  
}
