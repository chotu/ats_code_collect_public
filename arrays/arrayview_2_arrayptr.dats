(* array_v 2 arrayptr *)
(*
patscc -DATS_MEMALLOC_LIBC -o av2ap arrayview_2_arrayptr.dats
*)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"


macdef showtype (x) = $showtype ,(x)
macdef showlvaltype (x) = pridentity ($showtype ,(x))

extern
fun init_array{n:nat} (A : &(@[int?][n]) >> @[int][n] , n : int (n)) : void

implement
init_array{n} (A,n) = {
  implement array_initize$init<int> (i, x) = x := $UN.cast{int}(i)
  val () = array_initize<int>(A,i2sz(n))
}

extern
fun{a:t@ype}
dummy_take_arrayptr{n:nat} (A : !arrayptr(a,n) , n : int (n)) : void 

implement{a}
dummy_take_arrayptr (A,n) = {}


extern
fun add_100{n:nat} (A : !arrayptr(int,n) , n : int (n)) : void

implement
add_100{n} (A,n) = {
  implement(env) 
  array_foreach$fwork<int><env> (x,env) = x := (x+100)
  
  val _ = arrayptr_foreach (A,i2sz(n))
}
   



implement main0 () = {
  val out = stdout_ref
  val (pf , pfree | p)  = array_ptr_alloc<int> (i2sz(5))
  
  val () = init_array(!p,5)
  val () = (fprint_array (out,!p,i2sz(5)) ; println! ())
  
  (* val () = dummy_take_arrayptr<int> (!p,5) *)
  (* ^^^ : not supposed to work *)
  
  val A = arrayptr_encode (pf,pfree | p)
  val () = dummy_take_arrayptr<int> (A,5) 
  val () = add_100 (A,5) 
  val () = (fprint_arrayptr_sep (out,A,i2sz(5),",") ; println! ())
  val () = arrayptr_free (A)
  (* val () = array_ptr_free (pf,pfree | p) *)
  (* ^^^ : do not need this now *)
}
