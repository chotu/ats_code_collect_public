(* arrayptr to array (i.e array_v) *)

(*
patscc -DATS_MEMALLOC_LIBC -o ap2av arrayptr_2_arrayview.dats 
*)



#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"

macdef showtype (x) = $showtype ,(x)
macdef showlvaltype (x) = pridentity ($showtype ,(x))


extern 
fun{a:t@ype}
print_all_1{n:nat}
(A : &(@[a][n]) , n : int (n)) : void

implement{a}
print_all_1{n} (A,n) = {
  implement(env) array_foreach$fwork<a><env> (x , env) = 
    (print_val<a> (x) ; print(","))
  val _ = array_foreach (A,i2sz(n))
}



extern 
fun
add_100{n:nat}
(A : &(@[int][n]) , n : int (n)) : void

implement
add_100{n} (A,n) = {
  implement(env) array_foreach$fwork<int><env> (x , env) = x := x+100
  val _ = array_foreach (A,i2sz(n))
}
  
  


implement main0 () = {
  var A1 = @[int][5](1,2,3,4,5)
  val () = print_all_1<int> (A1,5)
  val () = println! ()
  
  val B = (arrayptr)$arrpsz{int}(0, 100, 200, 300, 400)
  
  (* val () = print_all_1<int> (B,5) *) 
  (* ^^^ : was not supposed to work *)
  
  
  val (pfav | p) = (arrayptr_takeout (B) | arrayptrout2ptr (B))  
  val () = print_all_1<int> (!p,5)
  val () = println! ()
  val () = add_100 (!p,5) 
  val () = print_all_1<int> (!p,5)
  val () = println! ()
  prval () = arrayptr_addback(pfav | B)  
  
  (* val () = print_all_1 (B,5)  *)
  (* ^^^ : was not supposed to work *)
  
  implement(env) array_foreach$fwork<int><env> (x , env) = 
    (print_val<int> (x) ; print(","))
  val _ = arrayptr_foreach<int> (B,i2sz(5))
  val () = println! ()
  val () = arrayptr_free(B)
  
}
