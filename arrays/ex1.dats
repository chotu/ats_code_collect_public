(* patscc -c ex1.dats *)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"

(* for convenience assume n > 0 *)
extern
fun 
foo_1{n:nat | n > 0} 
(A : &(@[int][n])) : void 

(* for convenience assume n > 0 *)
extern
fun
foo_2{n:nat | n > 0}{l:addr}
(pf : !array_v(int,l,n) | p : ptr (l) ) : void


implement
foo_1{n}(A) = 
  let   
    val () = println! (A.[0])
    val p = addr@A
    prval avw = view@A
    //val () = foo_2(avw | p)
    prval () = view@A := avw
  in end

implement
foo_2{n}{l} (pf | p) = 
  let 
        
    //prval (pf_at,pf_rest)  = array_v_uncons(pf)
    val x = p->[0] //ptr_get<int>(pf_at | p)    
    val () = foo_1 (!p)
    val () = println! (x)
    //prval () = pf := array_v_cons(pf_at,pf_rest)  
  in end

implement main0 () = {
  var tarr = @[int][10](100) 
  val () = foo_1(tarr)
  val (pf | p) = (view@tarr | addr@tarr)
  val () = foo_2(pf | p)
  prval () = view@tarr := pf
  
}
  

