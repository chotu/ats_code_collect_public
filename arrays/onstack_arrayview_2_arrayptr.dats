(* local array allocated on stack to arrayptr *)


(* 
  patscc  -o sav2p onstack_arrayview_2_arrayptr.dats
*)  


#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"


macdef showtype (x) = $showtype ,(x)
macdef showlvaltype (x) = pridentity ($showtype ,(x))


extern
fun{a:t@ype}
dummy_take_arrayptr{n:nat} (A : !arrayptr(a,n) , n : int (n)) : void 

implement{a}
dummy_take_arrayptr (A,n) = {}


extern
fun add_100{n:nat} (A : !arrayptr(int,n) , n : int (n)) : void

implement
add_100{n} (A,n) = {
  implement(env) 
  array_foreach$fwork<int><env> (x,env) = x := (x+100)
  
  val _ = arrayptr_foreach (A,i2sz(n))
}


extern 
fun{a:t@ype}
dummy_take_array{n:nat} (A : &(@[a][n]) , n : int (n)) : void

implement{a}
dummy_take_array (A,n) = {}



extern praxi
objectify{a:t@ype}{n:nat}{l:addr} 
(array_v (a ,l,n) | !ptrlin (l) >> arrayptr(a,l,n))
:<prf> mfree_ngc_v (l)

extern praxi
unobjectify{a:t@ype}{n:nat}{l:addr} 
(mfree_ngc_v (l) | !arrayptr(a,l,n) >> ptrlin (l))
:<prf> array_v (a,l,n) 


extern
fun test_casts () : void



implement main0 () = {
  val out = stdout_ref
  var A = @[int][5](1,2,3,4,5)
  val () = dummy_take_array<int> (A,5)
  (* val () = dummy_take_arrayptr<int> (A,5) *)
    (* ^^^: should not work  *)  
    
  val () = 
    (fprint_array_sep (out,A,i2sz(5),",") ; println! ())
  
  val (pf | p) = (view@A | addr@A)
  
  val pq = ptr2ptrlin (p)
  prval pf_free_ngc = objectify (pf | pq)
  
  val () = dummy_take_arrayptr<int> (pq,5)  
  (* val () = dummy_take_array<int> (pq,5) *)
    (* ^^^: should not work *)  
    
  val () = (fprint_arrayptr_sep (out,pq,i2sz(5),","); println! ())
  val () = add_100 (pq,5)
  val () = (fprint_arrayptr_sep (out,pq,i2sz(5),","); println! ())
  
  prval pf = unobjectify (pf_free_ngc | pq)
 
  val p = ptrlin2ptr (pq)
  val () = dummy_take_array<int> (!p,5)  
  
  val () = view@A := pf          
  
  (* more tests *)
  val () = test_casts ()
  
    
}

(* using castfns *)

extern
castfn 
arrayview_2_arrayptr{a:t@ype}{l:addr}{n:nat}
(pf : array_v(a,l,n) | ptr (l) ) : (mfree_ngc_v (l) | arrayptr (a,l,n))

implement
arrayview_2_arrayptr (pf | p) = let
  val pq = ptr2ptrlin (p)
  prval pfree = objectify (pf | pq)   
  in (pfree | pq) end
 


extern
castfn 
arrayptr_2_arrayview{a:t@ype}{l:addr}{n:nat}
(pfree : mfree_ngc_v (l) | arrayptr (a,l,n) ) : 
(array_v(a,l,n) | ptr (l) )

implement
arrayptr_2_arrayview (pfree | lp) = let
  prval pf  = unobjectify (pfree | lp)
  val p = ptrlin2ptr (lp)
  in (pf | p) end
  



implement test_casts () = {
  var A = @[int][5](1)
  val (pfree | arr_ptr) = 
    arrayview_2_arrayptr (view@A | addr@A)

  val () = dummy_take_arrayptr<int> (arr_ptr,5)
  
  val (pf | p) = 
    arrayptr_2_arrayview (pfree | arr_ptr)

  val () = dummy_take_array<int> (!p,5)
  
  val () = view@A := pf
}
      

